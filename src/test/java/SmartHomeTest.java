import org.junit.Test;

import static org.junit.Assert.*;

public class SmartHomeTest {

    @Test
    public void shouldTurnsLightOnWhenSwitch203IsOn() {
        LightBulb lightBulb = new LightBulb211();
        Switch203OnStub switchLight = new Switch203OnStub();

        SmartHome smartHome = new SmartHome(lightBulb, switchLight);

        smartHome.run();
        assertTrue(switchLight.isOn());
        assertTrue(lightBulb.isOn());

    }

    @Test
    public void shouldTurnsLightOffWhenSwitch203IsOff() {
        LightBulb lightBulb = new LightBulb211();
        Switch203OffStub switchLight = new Switch203OffStub();

        SmartHome smartHome = new SmartHome(lightBulb, switchLight);

        smartHome.run();
        assertFalse(switchLight.isOn());
        assertFalse(lightBulb.isOn());
    }

    @Test
    public void shouldNotMakeCoffeeWhenStartClockIsRunning() {
        Clock startClock = () -> true;
        Clock endClock = () -> true;

        SmartHome smartHome = new SmartHome(new CoffeeMakerClock(startClock, endClock));

        assertFalse(smartHome.isMakkingCoffee());
        assertFalse(smartHome.isCoffeeDone());
    }

    @Test
    public void shouldMakeCoffeeWhenStartClockIsNotRunning() {
        Clock startClock = () -> false;
        Clock endClock = () -> true;

        SmartHome smartHome = new SmartHome(new CoffeeMakerClock(startClock, endClock));

        assertTrue(smartHome.isMakkingCoffee());
        assertFalse(smartHome.isCoffeeDone());
    }

    @Test
    public void shouldNotMakeCoffeeWhenEndClockIsNotRunning() {
        Clock startClock = () -> false;
        Clock endClock = () -> false;

        SmartHome smartHome = new SmartHome(new CoffeeMakerClock(startClock, endClock));

        assertFalse(smartHome.isMakkingCoffee());
        assertTrue(smartHome.isCoffeeDone());
    }

    @Test
    public void shoulLightBulbKeepOffWhenCoffeMakerClockIsOn() {
        LightBulb lightBulb = new LightBulb211();
        Switch203OffStub switchLight = new Switch203OffStub();

        Clock startClock = () -> false;
        Clock endClock = () -> true;

        SmartHome smartHome = new SmartHome(lightBulb, switchLight, new CoffeeMakerClock(startClock, endClock));

        smartHome.run();

        assertFalse(switchLight.isOn());
        assertFalse(lightBulb.isOn());

        assertTrue(smartHome.isMakkingCoffee());
        assertFalse(smartHome.isCoffeeDone());
    }
}
