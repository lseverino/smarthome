public class CoffeeMakerClock {

    Clock startClock;
    Clock endClock;

    public CoffeeMakerClock(Clock startClock, Clock endClock) {
        this.startClock = startClock;
        this.endClock = endClock;
    }

    public boolean isOn() {
        return !startClock.isRunning() && endClock.isRunning();

    }

    public boolean isCoffeeDone() {
        return !startClock.isRunning() && !endClock.isRunning();
    }
}
