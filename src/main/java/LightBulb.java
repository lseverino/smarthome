public interface LightBulb {

   void turnOn();

    void turnOff();

    boolean isOn();
}
