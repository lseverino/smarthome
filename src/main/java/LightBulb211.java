public class LightBulb211 implements LightBulb {

    private boolean on;

    public void turnOn() {
        this.on = true;
    }

    public void turnOff() {
        this.on = false;
    }

    public boolean isOn() {
        return this.on;
    }
}
