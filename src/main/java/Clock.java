@FunctionalInterface
public interface Clock {
    boolean isRunning();
}
