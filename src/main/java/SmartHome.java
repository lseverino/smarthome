import static java.lang.System.out;

public class SmartHome {

    private LightBulb lightBulb;
    private SwitchLight switchLight;
    private CoffeeMakerClock coffeeMakerClock;

    public SmartHome(LightBulb lightBulb, SwitchLight switchLight) {
        this.lightBulb = lightBulb;
        this.switchLight = switchLight;
    }

    public SmartHome(CoffeeMakerClock coffeeMakerClock) {
        this.coffeeMakerClock = coffeeMakerClock;
    }

    public SmartHome(LightBulb lightBulb, SwitchLight switchLight, CoffeeMakerClock coffeeMakerClock) {
        this(lightBulb, switchLight);
        this.coffeeMakerClock = coffeeMakerClock;
    }

    public void run() {

        if (switchLight.isOn()) {
            lightBulb.turnOn();
        } else {
            lightBulb.turnOff();
        }

        out.println(">>>> RESULT: The Switch is [" + switchLight.isOn() + "] AND the LightBulb is [" + lightBulb.isOn() + "]");
    }

    public boolean isMakkingCoffee() {
        return coffeeMakerClock.isOn();
    }

    public boolean isCoffeeDone() {
        return coffeeMakerClock.isCoffeeDone();
    }
}
